const axios = require('axios');


const insultUrl = 'https://insult.mattbas.org/api/insult.json';

const makeTranslateUrl = (text, lang) => `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${process.env.YANDEX_TOKEN}&text=${text}&lang=en-${lang}`;

const translateRequest = async (text, lang) => axios.post(makeTranslateUrl(text, lang));

const fetchRandomInsult = async (lang) => {
  const insultResp = await axios.get(insultUrl);
  const translatedInsultResp = await translateRequest(insultResp.data.insult, lang || 'ru');
  return translatedInsultResp.data.text[0];
};


module.exports = fetchRandomInsult;
