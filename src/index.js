const dotenv = require('dotenv');
const Discord = require('discord.js');
const logger = require('./logger');
const processor = require('./message-processor');
const listeners = require('./listeners');

dotenv.config({ path: '../.env' });

const client = new Discord.Client();
const token = process.env.BOT_TOKEN;

const messageProcessor = new processor.MessageProcessor();
messageProcessor.registerListeners(listeners);

client.on('message', (message) => {
  messageProcessor.processMessage(message);
});

client.once('ready', () => {
  logger.info('Ready to process messages');
});


client.login(token);
