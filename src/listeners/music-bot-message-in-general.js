const logger = require('../logger');
const fetchRandomInsult = require('../insult');
const {
  musicBotPrefixes,
  musicBotCmds,
  musicBotChannels,
} = require('../config').musicBotWatcherConfig;


const isBotCommand = (text) => musicBotPrefixes.some((prefix) => musicBotCmds.some((cmd) => text.startsWith(`${prefix}${cmd}`)));


module.exports = {
  name: 'music-bot-guard',
  description: 'Tag a member and kick them (but not really).',
  test: (msg) => isBotCommand(msg.content) && !musicBotChannels.includes(msg.channel.name),
  execute: async (message) => {
    const insult = await fetchRandomInsult();
    await message.reply(insult);
    logger.info(`insult sent to ${message.author.username}`);
  },
};
