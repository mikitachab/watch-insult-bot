const logger = require('./logger');

class MessageProcessor {
  constructor() {
    this.listeners = [];
  }

  registerListener(listener) {
    this.listeners.push(listener);
  }

  registerListeners(listeners) {
    listeners.forEach((listener) => this.registerListener(listener));
  }

  processMessage(message) {
    this.listeners.forEach((listener) => {
      try {
        if (message.channel.type === 'text' && listener.test(message)) {
          logger.info(`triggered ${listener.name} on ${message.guild}#${message.channel.name}:${message.author.username}`);
          listener.execute(message);
        }
      } catch (error) {
        logger.error(`failed to process message with ${listener.name}`);
        logger.error(error);
      }
    });
  }
}


module.exports = { MessageProcessor };
